module u9k

go 1.14

require (
	github.com/domodwyer/mailyak/v3 v3.2.2
	github.com/gabriel-vasile/mimetype v1.4.0 // indirect
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/httprate v0.4.0
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/hako/durafmt v0.0.0-20200710122514-c0fb7b4da026
	github.com/hashicorp/go-multierror v1.1.1
	github.com/jackc/pgconn v1.12.1
	github.com/jackc/pgerrcode v0.0.0-20201024163028-a0d42d470451
	github.com/jackc/pgx/v4 v4.16.1
	github.com/rhnvrm/simples3 v0.6.1
	github.com/xojoc/useragent v0.0.0-20200116211053-1ec61d55e8fe
	go.uber.org/atomic v1.7.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
