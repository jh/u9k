#!/bin/bash

set -e

version="$1"

if echo "$version" | grep -E '^v[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+$' >/dev/null; then
    echo "Releasing version '$version'"
else
    echo "Invalid release tag '$version': Expected format: 'v0.0.0'"
    exit 1
fi

if [ $(git diff --cached | wc -l) != '0' ]; then
    echo "You have staged changes! Aborting release"
    exit 1
fi

sed -i "s/const Version = \".*\"\$/const Version = \"$version\"/" config/config.go
git add config/config.go
git commit -m "Release version $version"
git tag "$version"

echo "+++ Run the following command to publish the release +++"
echo "git push --atomic origin master ${version}"
