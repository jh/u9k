# Makefile for u9k

.PHONY: server
server: # server creates the u9k server binary
	CGO_ENABLED=0 go build ./cmd/server

.PHONY: container
container:
	podman image build -t docker.io/jacksgt/u9k:dev .

.PHONY: test
go-tests: # runs all the tests (requires DB / object store)
	. ./auth/dev-env.sh && go test ./...

.PHONY: go-unit-tests
go-unit-tests:
	go test -test.short ./...

.PHONY: js-lint
js-lint:
	( PATH=./node_modules/.bin/:$$PATH node_modules/.bin/eslint static/js/ )

.PHONY: dev-env # sets up the development environment (postgres and minio)
dev-env:
    # if the container already exists, the "create" step will fail, but we ignore the error with the "-" prefix
	-podman container create \
    --name u9k-postgres \
	-p 127.0.0.1:5432:5432 \
	-e POSTGRES_DB=u9k-dev \
	-e POSTGRES_USER=u9k-dev \
	-e POSTGRES_PASSWORD=dev-password \
	-u 999 \
	docker.io/library/postgres:14-bullseye

	-podman container create \
    --name u9k-minio \
	-p 127.0.0.1:9000:9000 \
	-p 127.0.0.1:9001:9001 \
	-e MINIO_ROOT_USER=u9k-dev \
	-e MINIO_ROOT_PASSWORD=dev-password \
	-e MINIO_DEFAULT_BUCKETS=u9k-dev,simples3 \
	docker.io/bitnami/minio:2022.5.26

	podman container start u9k-postgres u9k-minio

	# to reset the stored data, delete the containers with:
	# podman container rm -f u9k-{minio,postgres}

.PHONY: dev # runs the development version
dev:
	. ./auth/dev-env.sh && go run cmd/server/main.go -reloadTemplates=true

.PHONY: debug # runs the development version with delve debugger
debug:
	. ./auth/dev-env.sh && dlv debug cmd/server/main.go
